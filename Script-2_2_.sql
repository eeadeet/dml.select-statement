SELECT
    f.title AS movie_title,
    COUNT(r.rental_id) AS rental_count,
    AVG(EXTRACT(YEAR FROM current_date) - EXTRACT(YEAR FROM c.create_date)) AS expected_age
FROM
    film AS f
JOIN
    inventory AS i ON f.film_id = i.film_id
JOIN
    rental AS r ON i.inventory_id = r.inventory_id
JOIN
    customer AS c ON r.customer_id = c.customer_id
GROUP BY
    f.title
ORDER BY
    rental_count DESC
LIMIT 5;
