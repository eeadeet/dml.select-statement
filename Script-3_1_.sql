SELECT
    a.actor_id,
    a.first_name,
    a.last_name,
    MAX(f.release_year) - MIN(f.release_year) AS acting_duration,
    (EXTRACT(YEAR FROM current_date) - MAX(f.release_year)) AS inactive_years
FROM
    actor AS a
LEFT JOIN
    film_actor AS fa ON a.actor_id = fa.actor_id
LEFT JOIN
    film AS f ON fa.film_id = f.film_id
GROUP BY
    a.actor_id, a.first_name, a.last_name
ORDER BY
    inactive_years DESC;
