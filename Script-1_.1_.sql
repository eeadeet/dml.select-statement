SELECT
    s.store_id,
    s.manager_staff_id,
    CONCAT(st.first_name, ' ', st.last_name) AS manager_name,
    SUM(p.amount) AS total_revenue
FROM
    store AS s
JOIN
    staff AS st ON s.manager_staff_id = st.staff_id
JOIN
    payment AS p ON st.staff_id = p.staff_id
WHERE
    EXTRACT(YEAR FROM p.payment_date) = 2017
GROUP BY
    s.store_id, s.manager_staff_id, st.first_name, st.last_name
HAVING
    SUM(p.amount) = (
        SELECT
            MAX(total_revenue)
        FROM
            (SELECT
                s.store_id,
                s.manager_staff_id,
                SUM(p.amount) AS total_revenue
            FROM
                store AS s
            JOIN
                staff AS st ON s.manager_staff_id = st.staff_id
            JOIN
                payment AS p ON st.staff_id = p.staff_id
            WHERE
                EXTRACT(YEAR FROM p.payment_date) = 2017
            GROUP BY
                s.store_id, s.manager_staff_id) AS max_revenue
        WHERE
            max_revenue.store_id = s.store_id
    );
